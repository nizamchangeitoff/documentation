# Preview your Website

Just click on the project icon on your dashboard to preview your website!

![preview](images/preview.jpg)

This will show the cached version of your website.

!!! note
    The cache is updated every 24 hours. So if you want to preview after [making changes](), select the option of "preview (w/o CDN)" 

###Preview (w/o CDN)
 Preview without CDN option is present in the menu at the top right of your project.

![preview_option](images/preview_option.png)        

You can also share the preview link with your client for approval before publishing.

Sample preview link:

![https://d1y3s8cye6541c.cloudfront.net/5970498738728374409b06b7/index.html](images/preview_link.png)        
