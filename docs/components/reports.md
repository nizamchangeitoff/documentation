# Reporting and Engagement

Reports is a component in Kitsune which enables reporting and proactive engagement emails to be sent to your customers. You can configure what and when emails should be sent through this component. You also have complete control over the email template. Edit it the same way you edit HTML pages in Kitsune and you are good to go. Data-points of the reports are provided to you by a system defined [ language](#report-language-reference) which you can refer to in the language editor section of the IDE.

## Enabling Reports

#### Step 1:

Open your Kitsune project. Under 'components' menu, click on 'reports'. Click 'enable' in the confirmation dialog. This will add the required files to your project automatically and will also enable the reporting service. A new folder '__components' will be added with a subfolder 'reports' under which your files will be located. 

#### Step 2:

Design an email template which you want to send as a report. Follow the system defined [language](#report-language-reference) to know what is being exposed and how you can access it. Update that email template file name in `kitsune-settings.json` under `reports` section. 

**Note:** A predefined ready-to-use performance report will be added to your project under the '__components/reports/templates' folder. It makes use of the [language](#report-language-reference) properties to the full extent to send a good looking report to your customers. 

#### Step 3:

Once you are done with your customizations (logo etc.), you can *build* the project and check the preview by *right click > preview page* on that file in the left panel. 

## Report settings reference

#### Root object

| Property name            | Date type | Applicable values                    | Description                                                                                                                                   |
|:-------------------------|:----------|:-------------------------------------|:----------------------------------------------------------------------------------------------------------------------------------------------|
| notifications            | array     | Array of [NotificationItem](#notificationitem-object)    | An array of notifications you want to send to your customers. For details on each notification see notification item reference                |
| settings                 | object    | See [Settings](#settings-object) reference               | Settings to be used by the reports component like custom email SMTP details etc.                                                                                |

#### NotificationItem object

| Property name            | Date type | Applicable values                    | Description                                                                                                                                   |
|:-------------------------|:----------|:-------------------------------------|:----------------------------------------------------------------------------------------------------------------------------------------------|
| type                     | string    | `PERIODIC_PERFORMANCE_REPORT`      | Periodic Performance Report sends information like visits, visitors, top traffic sources and most visited pages periodically to your customers |
| period                   | number    | No. of days                          | Specifies the period of periodic reports in no of days                                                                                        |
| email                    | object    | See [NotificationItem.Email](#notificationitememail-object) reference | Specifies email notification details                                                                                                          |

#### NotificationItem.Email object

| Property name            | Date type | Applicable values                    | Description                                                                                                                                   |
|:-------------------------|:----------|:-------------------------------------|:----------------------------------------------------------------------------------------------------------------------------------------------|
| body                     | string    | Valid HTML file path                 | The path of HTML template file to be used as email body - relative to the root directory                                                      |

#### Settings object

| Property name            | Date type | Applicable values                    | Description                                                                                                                                   |
|:-------------------------|:----------|:-------------------------------------|:----------------------------------------------------------------------------------------------------------------------------------------------|
| email                    | object    | See [Settings.Email](#settingsemail-object) reference         | Custom email SMTP details to be used for sending emails                                                                                       |

#### Settings.Email object
| Property name            | Date type | Applicable values                    | Description                                                                                                                                   |
|:-------------------------|:----------|:-------------------------------------|:----------------------------------------------------------------------------------------------------------------------------------------------|
| host                     | string    | Valid SMTP host                      | The host provided by your email service provider                                                                                              |
| port                     | number    | Valid SMTP port                      | The port provided by your email service provider                                                                                              |
| username                 | string    | Non-empty string                     | The username provided by your email service provider                                                                                          |
| password                 | string    | Non-empty string                     | The password provided by your email service provider                                                                                          |
| ssl_enabled              | boolean   | true/false                           | Enable or disable SSL                                                                                                                         |

Sample `reports` section in `kitsune-settings.json` for enabling periodic performance reports to be sent to your customers every 7 days.

```json
{
  ...
  "reports": {
        "notifications": [
            {
                "type": "PERIODIC_PERFORMANCE_REPORT",
                "period": 7,
                "email": {
                    "body": "periodic_performance_report.html"
                }
            }
        ],
        "settings": {
            //Fill the details below to use your own email service provider or remove this section to use Kitsune's default email service provider
            "email": {
                "host": "",
                "port": 0,
                "username": "",
                "password": "",
                "ssl_enabled": true
            }
        }
    }
  ...
}
```

### Report language reference

Reports component gets its data from reports language. Data like visits, visitors etc is provided to you by reports language so that you can use them out of the box and focus only on the design of the email.

All the properties of reports language can be accessed using `report.<property_name>`

The following are available properties provided by reports language :

#### Report object

| Property name                 | Date type            | Description                                                                                                                                          |
|:------------------------------|:---------------------|:-----------------------------------------------------------------------------------------------------------------------------------------------------|
| website_domain                | string               | Domain of the customer                                                                                                                               |
| website_performance     | [PeriodicPerformanceStatistics](#periodicperformancestatistics-object) | Website performance statistics in current and previous window  |

#### PeriodicPerformanceStatistics object
| Property name                 | Date type            | Description                                                                                                                                          |
|:------------------------------|:---------------------|:-----------------------------------------------------------------------------------------------------------------------------------------------------|
| period                        | number               | Performance report period in days given in kitsune-settings.json and it defines the window size                                                                                     |
| current_window                | [PerformanceStatistics](#performancestatistics-object)| Performance statistics in the current window                                                                                  |
| previous_window               | [PerformanceStatistics](#performancestatistics-object)| Performance statistics in the previous window                                                                                                        |

#### PerformanceStatistics object
| Property name                 | Date type            | Description                                                                                                                                          |
|:------------------------------|:---------------------|:-----------------------------------------------------------------------------------------------------------------------------------------------------|
| date_range                    | [DateRange](#daterange-object) | The start date and end date of the window                                                                                                  |
| visits_count                  | number               | Number of visits in the window                                                                                                                   |
| visitors_count                | number               | Number of visitors in the window                                                                                                                 |
| top_referrers                 | array of [TopReferrersItem](#topreferrersitem-object)     | List of top referrers from which your website got the traffic in the window  |
| total_visits_from_top_referrers | number               | Total visits received from the top referrers in the window                                                                                        |
| most_visited_pages            | array of [MostVisitedPagesItem](#mostvisitedpagesitem-object) | List of the most visited pages in the window                                                                                                 |
| total_visits_from_most_visited_pages | number               | Total visits received from the most visited pages in the window                                                                                   |

#### TopReferrersItem object
| Property name                 | Date type            | Description                                                                                                                                          |
|:------------------------------|:---------------------|:-----------------------------------------------------------------------------------------------------------------------------------------------------|
| source_website                | string               | The source website of the visits                                                                                                                     |
| visits_count                  | number               | No of visits received from the source website                                                                                                        |

#### MostVisitedPagesItem object
| Property name                 | Date type            | Description                                                                                                                                          |
|:------------------------------|:---------------------|:-----------------------------------------------------------------------------------------------------------------------------------------------------|
| page                          | string               | Page path                                                                                                                                             |
| visits_count                  | number               | No of visits to this page                                                                                                                            |

#### DateRange object
| Property name                 | Date type            | Description                                                                                                                                          |
|:------------------------------|:---------------------|:-----------------------------------------------------------------------------------------------------------------------------------------------------|
| start_date                    | string               | Start date                                                                                                                                           |
| end_date                      | string               | End date                                                                                                                                             |