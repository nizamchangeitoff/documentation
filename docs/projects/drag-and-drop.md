# Drag and Drop

Simply drag and drop your website folder on the dashboard and all the files will be added as a project.


![dragDrop](/images/projects/projectUploadSign.png)
        
You will be asked to give your project a name.

![project_name](/images/gettingStarted/projectName.png)
        
When the website is uploaded successfully, click on the optimize button on the pop up that appears. It will optimize all your HTML, CSS, JS and other assets.

![optimize_choose](/images/gettingStarted/optimizeChoose.png)

![optimize_assets](/images/gettingStarted/optimizeStarted.png)

After successful optimization & upload, you will be able to see your project on the dashboard.

![dashboard](/images/gettingStarted/uploadDone.png)