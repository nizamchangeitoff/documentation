# How to upload your first website on Kitsune??

You can upload a new static website on Kitsune just by drag and drop of all the files!

How? Just follow the steps to host a simple static website and make it live!

**Step 1** Login with your Google account by going to [Login](https://dashboard.kitsune.tools/login)

<p  align="center">
    <img src = "/images/gettingStarted/kitsune-login.png" alt="kitsune">
</p>

**Step 2**. Download the zip file of the sample website template from [here](/assets/coffeewebsitetemplate.zip). Unzip this file. It contains a few simple pages like index, contact, about-us, price, services along with style-sheets, scripts, and images.

![website_template](/images/temp/website_template12.png)

**Step 3**. Click on the top right **+** Button for opening the new project pop up. 

![1_newProj](/images/gettingStarted/createNewProject1.png)


![2_newProj](/images/gettingStarted/createNewProject2.png)


**Step 4**. Drag and drop the entire extracted website folder onto the [Kitsune dashboard](https://dashboard.kitsune.tools/projects) and let the magic begin :)


![1_migrate](/images/gettingStarted/folderUpload.png)



But wait! Before the magic starts...

**Step 5**. Give the project a name and click on **create and start uploading**.


<p  align="center">
    <img src = "/images/gettingStarted/projectName.png" alt="newProjectName">
</p>

and see the website been created.

<p  align="center">
    <img src = "/images/gettingStarted/uploadDone.png" alt="uploadDone">
</p>

!!! Success 
    **Kudos**!! Time for some cheering up! You made your first website in **Kitsune**. Pat your back for some time before we move on to the next topic.