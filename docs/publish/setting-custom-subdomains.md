# Mapping your domain to Kitsune

There are two options here:

* Root domain e.g. **yourdomain.com**
* Subdomain e.g. **subdomain.yourdomain.com**

## Root Domain Mapping
Depending on your DNS provider there are two ways to do this.

* Set a CNAME record
* Set an A record and CNAME record

## CNAME Record for Root Domain
* Set a **CNAME**  of `@` or `yourdomain.com` to `<target>.getkitsune.com`

![CNAME](/images/publish/cnameCreate.png)

## A Record for Root Domain
Certain DNS providers allow only setting a **A record**. If it's the same with your DNS provider then:

* Set an **A record** from `@` or `yourdomain.com` to **35.154.83.253**
* Set a **CNAME** from `www` to `yourdomain.getkitsune.com`

![A](/images/publish/aRecordChoice.png)
![A](/images/publish/cnameToRoot.png)

## Subdomain Mapping
For a subdomain like `your.domain.com`

* Set a **CNAME** from `your` to `<target>.getkitsune.com`

![A](/images/publish/subDomainMapping.png)

You can get the `<target>.getkitsune.com` link from live sites section on the dashboard.

## DNS Records Verification

After you have added all the data and records to your DNS of the domain. Now is the time to verify all the input records and check whether they are correctly mapping or not.

* For that **DNS** Records can be verified by visiting :

    - [MXLookUp](https://mxtoolbox.com/)
    - [ViewDNS](https://viewdns.info/)
    - [DNSChecker](https://dnschecker.org/)