#Why Optimize your website?

People are more likely to visit your website if it loads faster. Thus optimizing websites for speed and performance is critical, but if you have done enough of that then you know that it is very **boring** and **time consuming**. So we decided to take it off your hands. 

Kitsune optimizes your HTML, CSS, JS and other assests on just a click of mouse.

##CDN
Content Delivery Network is a system of distributed data centers or servers which delivers content according to the user's geographical location, origin of web content, and server delivering the content. Kitsune reduces latency, handles traffic spike, creates cached copy of your site by providing CDN services. So that your site loads faster from anywhere in world.

##Minification
Minification removes all unnecessary characters from the source code without changing its functionality, making the website fast, reducing resource usage and lowering the bandwidth cost.

##GZIP
GZIP compresses all your pages and other content on the server before sending it to the browser, thus lowering bandwidth costs and giving faster loading websites.

##Versioning
Browser caching creates the problem of file invalidation. If older files are not invalidated regularly then new changes won't be reflected on the website. Kitsune also handles versioning of all your files, so that when a new version is uploaded your browser fetches the new file from the server rather than rendering the cached copy.

##SEO
Kitsune helps improve your Search Engine Optimization, by smart keyword extraction and placing those keywords in the footer of your website, helping in ranking on search engines. 
Also you know that the websites updated regularly are the ones ranked higher in search results, but updating them for each client is a pain. What if they could update the website themselves with a few clicks of mouse? 

Kitsune comes with a website management portal called as **k-admin** for instant updates.